'use strict';

const { isDraft } = require('strapi-utils').contentTypes;

module.exports = {


  async create(data, { files } = {}) {
    const validData = await strapi.entityValidator.validateEntityCreation(
      strapi.models.tasks,
      data,
      { isDraft: isDraft(data, strapi.models.tasks) }
    );

    const entry = await strapi.query('tasks').create(validData);

    if (files) {
      // automatically uploads the files based on the entry and the model
      await strapi.entityService.uploadFiles(entry, files, {
        model: 'tasks',
        // if you are using a plugin's model you will have to add the `source` key (source: 'users-permissions')
      });
      return this.findOne({ id: entry.id });
    }

    return entry;
  },
};
