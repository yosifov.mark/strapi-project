'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');


 module.exports = {
    
  async complete(ctx) {
      let entity;
      let taskData 
      let task 
      const { id } = ctx.params
      
      async function completeTask(){
         taskData = await strapi.query('completedtasks').find({ task: id ,users_permissions_user:  ctx.state.user.id}).then(data => taskData = data)
         task = await strapi.query('tasks').find({ id: id }).then(data => task = data)
        
         if (task[0] === undefined){
            return ctx.response.badRequest(409, "Task is missing")
         }
         if (taskData[0] !== undefined){
            return ctx.response.badRequest(409, "Task is already completed")
         }
          else {
              if (ctx.is('multipart')) {
               const { data, files } = parseMultipartData(ctx);
               entity = await strapi.services.completedtasks.create(data, { files });
              } else {
               entity = await strapi.services.completedtasks.create(
                 { task:id, users_permissions_user: ctx.state.user.id, is_completed: 1 } );
              }
        
               return sanitizeEntity(entity, { model: strapi.models.completedtasks });
         }
          
       }
        
        return completeTask();
       
      
      
      },
 };
