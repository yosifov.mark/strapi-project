'use strict';

const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

module.exports = {
 

  async create(ctx) {
    let entity;
    let taskData
    let userData
    let task
 
   async function validation(){
     taskData = await strapi.query('completedtasks').find({ task: ctx.request.body.task }).then(data => taskData = data)
     userData = await strapi.query('completedtasks').find({ users_permissions_user: ctx.request.body.users_permissions_user }).then(data => userData = data)
     task = await strapi.query('tasks').find({ id: ctx.request.body.task }).then(data => task = data)
     
      
      if (taskData[0] && userData[0]){
        return ctx.response.badRequest(409, "Task is already completed")
      }
      else if(task[0].id == undefined){
        return ctx.response.badRequest(409, "Task is missing ")
      }
      else {
          if (ctx.is('multipart')) {
           const { data, files } = parseMultipartData(ctx);
           entity = await strapi.services.completedtasks.create(data, { files });
          } else {
           entity = await strapi.services.completedtasks.create(ctx.request.body);
          }
    
           return sanitizeEntity(entity, { model: strapi.models.completedtasks });
      }
      
   }
    return validation();
  },
};

